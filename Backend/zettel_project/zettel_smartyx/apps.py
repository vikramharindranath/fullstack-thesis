from django.apps import AppConfig


class ZettelSmartyxConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'zettel_smartyx'
