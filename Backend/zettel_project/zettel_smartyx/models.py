from django.db import models
from datetime import datetime
import datetime

# Create your models here.


class GetImage(models.Model):
    title = models.CharField(max_length=200, default=datetime.datetime.now())
    image = models.ImageField()
    image2 = models.ImageField(null=True)

    def __str__(self):
        return self.title


class GetOutput(models.Model):
    name = models.CharField(max_length=800, null=True)
    price = models.FloatField(default="0.00", null=True)
    place = models.CharField(max_length=2000, null=True)
    date = models.CharField(default=datetime.date.today, max_length=200)

    def __str__(self):
        return self.name
