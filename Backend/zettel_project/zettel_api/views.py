from django.db import models
from django.shortcuts import render
from rest_framework import generics
from rest_framework import filters
from zettel_smartyx import models
from .serializers import GetImageSerializer, GetOutputSerializer
from rest_framework.pagination import LimitOffsetPagination
from rest_framework.filters import OrderingFilter, SearchFilter


class GetImageView(generics.ListCreateAPIView):
    queryset = models.GetImage.objects.all()
    serializer_class = GetImageSerializer
    filter_backends = [filters.OrderingFilter]
    ordering_fields = ['id']
    pagination_class = LimitOffsetPagination


class GetOutputView(generics.ListCreateAPIView):
    queryset = models.GetOutput.objects.all()
    serializer_class = GetOutputSerializer
    filter_backends = [filters.OrderingFilter]
    ordering_fields = ['id']
    pagination_class = LimitOffsetPagination


class DeleteImageView(generics.RetrieveUpdateDestroyAPIView):
    queryset = models.GetImage.objects.all()
    serializer_class = GetImageSerializer
    #filter_backends = (OrderingFilter)
    #ordering = ('id')
