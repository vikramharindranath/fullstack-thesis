from django.urls import path
from django.urls.resolvers import URLPattern
from django.conf.urls.static import static, serve
from django.conf import settings
from .views import GetImageView, DeleteImageView, GetOutputView

urlpatterns = [
    path('incoming/', GetImageView.as_view(), name='incoming'),
    path('finished/', GetOutputView.as_view(), name='finished'),
    path('<int:pk>/', DeleteImageView.as_view())
]
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
