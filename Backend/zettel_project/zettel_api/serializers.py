from rest_framework import serializers
from zettel_smartyx import models


class GetImageSerializer(serializers.ModelSerializer):
    class Meta:
        fields = (
            'id',
            'title',
            'image',
            'image2',
        )
        model = models.GetImage


class GetOutputSerializer(serializers.ModelSerializer):
    class Meta:
        fields = (
            'id',
            'name',
            'price',
            'place',
            'date',
        )
        model = models.GetOutput
