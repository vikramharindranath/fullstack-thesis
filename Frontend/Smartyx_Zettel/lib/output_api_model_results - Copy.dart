// To parse this JSON data, do
//
//     final finalResult = finalResultFromJson(jsonString);

import 'dart:convert';

List<FinalResult> finalResultFromJson(String str) => List<FinalResult>.from(json.decode(str).map((x) => FinalResult.fromJson(x)));

String finalResultToJson(List<FinalResult> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class FinalResult {
    FinalResult({
        required this.id,
        required this.name,
        required this.price,
        required this.place,
        required this.date,
    });

    int id;
    String? name;
    double? price;
    String? place;
    DateTime date;

    factory FinalResult.fromJson(Map<String, dynamic> json) => FinalResult(
        id: json["id"],
        name: json["name"],
        price: json["price"].toDouble(),
        place: json["place"] == null ? null : json["place"],
        date: DateTime.parse(json["date"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "price": price,
        "place": place == null ? null : place,
        "date": "${date.year.toString().padLeft(4, '0')}-${date.month.toString().padLeft(2, '0')}-${date.day.toString().padLeft(2, '0')}",
    };
}
