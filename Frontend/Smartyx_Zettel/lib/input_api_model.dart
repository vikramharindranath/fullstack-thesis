// To parse this JSON data, do
//
//     final inputImage = inputImageFromJson(jsonString);

import 'dart:convert';

List<InputImage> inputImageFromJson(String str) => List<InputImage>.from(json.decode(str).map((x) => InputImage.fromJson(x)));

String inputImageToJson(List<InputImage> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class InputImage {
    InputImage({
        required this.id,
        required this.title,
        required this.image,
        required this.image2,
    });

    int id;
    String title;
    String image;
    String? image2;

    factory InputImage.fromJson(Map<String, dynamic> json) => InputImage(
        id: json["id"],
        title: json["title"],
        image: json["image"],
        image2: json["image2"] == null ? null : json["image2"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "title": title,
        "image": image,
        "image2": image2 == null ? null : image2,
    };
}
