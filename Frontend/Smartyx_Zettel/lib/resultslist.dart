import 'dart:convert';
import 'dart:io';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart'; 
import 'package:smartyx_receipts/output_api_model_results.dart';
import 'package:http/http.dart' as http;
import 'package:smartyx_receipts/storage_page.dart';
import 'package:csv/csv.dart';
import 'package:random_string/random_string.dart';
import 'package:syncfusion_flutter_xlsio/xlsio.dart' as sff;
import 'package:open_file/open_file.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart' as dt;


class PrintingResultsPage extends StatefulWidget {
  const PrintingResultsPage({Key? key}) : super(key: key);

  @override
  _PrintingResultsPageState createState() => _PrintingResultsPageState();
}

class _PrintingResultsPageState extends State<PrintingResultsPage> {
  String? stringResponse;
  //GetResult2Class _respJson = GetResult2Class(results: []);
  int _code = 0;
  TextEditingController nameController = TextEditingController();
  TextEditingController resultNameController = TextEditingController();
  TextEditingController priceController = TextEditingController();
  TextEditingController placeController = TextEditingController();
  TextEditingController dateController = TextEditingController();

  Dio dio = new Dio();

  Future getResultFromApi() async { 

      dynamic mapResponse;

      final outputResponse = await http.get(Uri.http('192.168.0.107:8000', 'finished', {'ordering' : '-id'}));
      print(outputResponse.statusCode);
      if(outputResponse.statusCode == 200) {
        mapResponse = json.decode(outputResponse.body);
        print(mapResponse[0]);
        var finalResponse = mapResponse[0];
        FinalResult getResult = FinalResult.fromJson(finalResponse);

        nameController.text = getResult.name.toString();
        priceController.text = getResult.price.toString();
        placeController.text = getResult.place.toString();
        dateController.text = getResult.date.toString();
        
      } else {
        throw Exception('Failed to load post');
      }
  }

  Future<void> createExcel() async {
    final sff.Workbook workbook = sff.Workbook();
    final sff.Worksheet sheet = workbook.worksheets[0];
    sheet.getRangeByName('A1').setText('Fields');
    sheet.getRangeByName('B1').setText('Data');
    sheet.getRangeByName('A2').setText('Name');
    sheet.getRangeByName('B2').setValue(nameController.text);
    sheet.getRangeByName('A3').setText('Price');
    sheet.getRangeByName('B3').setValue(priceController.text);
    sheet.getRangeByName('A4').setText('Place');
    sheet.getRangeByName('B4').setValue(placeController.text);
    sheet.getRangeByName('A5').setText('Date');
    sheet.getRangeByName('B5').setValue(dateController.text);
    final List<int> bytes = workbook.saveAsStream();
    workbook.dispose();

    final String path = (await getApplicationSupportDirectory()).path;
    final String fileName = '$path/Extraction.xlsx';
    final File file = File(fileName);
    await file.writeAsBytes(bytes, flush: true);
    OpenFile.open(fileName);
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        body: Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage('assets/bgdark.jpg'), fit: BoxFit.cover),
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                new Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.only(
                            left: 110.0, right: 110.0, top: 10.0),
                        child: GestureDetector(
                          onTap: () {
                            getResultFromApi();
                            //Navigator.push(
                            //context,
                            //MaterialPageRoute(
                            //builder: (context) => ();
                            //));
                          },
                          child: new Container(
                              alignment: Alignment.center,
                              height: 50.0,
                              width: 10.0,
                              decoration: new BoxDecoration(
                                  color: Color(0xFF16B0C4).withOpacity(0.7),
                                  borderRadius:
                                      new BorderRadius.circular(30.0)),
                              child: new Text("Fetch Result",
                                  style: new TextStyle(
                                      fontSize: 20.0, color: Colors.white))),
                        ),
                      ),
                    )
                  ],
                ),
                SizedBox(
                  height: 35,
                ),
                //if (_code == 200)
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    SizedBox(
                      height: 20,
                    ),
                    new Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Image(
                          image: AssetImage("assets/logo.png"),
                          width: 140,
                          height: 120,
                          alignment: Alignment.center,
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 30,
                    ),
                    Container(
                      alignment: Alignment.topLeft,
                      padding: EdgeInsets.only(left: 30.0),
                      child: InkWell(
                        child: Text(
                          'Name of the Organisation',
                          style: TextStyle(
                            fontSize: 18.0,
                            color: Colors.white.withOpacity(0.8),
                            fontStyle: FontStyle.italic,
                            fontWeight: FontWeight.bold,
                            fontFamily: 'Montserrat',
                          ),
                        ),
                      ),
                    ),
                    new Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Expanded(
                          child: Padding(
                            padding: const EdgeInsets.only(
                                left: 20.0, right: 20.0, top: 10.0),
                            child: GestureDetector(
                                onTap: () {},
                                child: new TextField(
                                  controller: nameController,
                                  textAlign: TextAlign.center,
                                  obscureText: false,
                                  keyboardType: TextInputType.text,
                                  style: TextStyle(color: Colors.white),
                                  decoration: InputDecoration(
                                    focusedBorder: OutlineInputBorder(
                                        borderSide: BorderSide(
                                            color: Color(0xFF16B0C4)
                                                .withOpacity(0.7),
                                            width: 5.0),
                                        borderRadius: const BorderRadius.all(
                                            const Radius.circular(20.0))),
                                    enabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(
                                            color: Color(0xFF16B0C4)
                                                .withOpacity(0.7),
                                            width: 5.0),
                                        borderRadius: const BorderRadius.all(
                                            const Radius.circular(20.0))),
                                    //hintText: "Markt",
                                    hintStyle: new TextStyle(
                                      fontFamily: 'Cairo',
                                      fontStyle: FontStyle.italic,
                                      fontWeight: FontWeight.normal,
                                      color: Colors.white,
                                    ),
                                  ),
                                )),
                          ),
                        )
                      ],
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    Container(
                      alignment: Alignment.topLeft,
                      padding: EdgeInsets.only(left: 30.0),
                      child: InkWell(
                        child: Text(
                          'Price',
                          style: TextStyle(
                            fontSize: 18.0,
                            color: Colors.white.withOpacity(0.8),
                            fontStyle: FontStyle.italic,
                            fontWeight: FontWeight.bold,
                            fontFamily: 'Montserrat',
                          ),
                        ),
                      ),
                    ),
                    new Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Expanded(
                          child: Padding(
                            padding: const EdgeInsets.only(
                                left: 20.0, right: 20.0, top: 10.0),
                            child: GestureDetector(
                                onTap: () {},
                                child: new TextField(
                                  controller: priceController,
                                  textAlign: TextAlign.center,
                                  keyboardType: TextInputType.number,
                                  obscureText: false,
                                  style: TextStyle(color: Colors.white),
                                  decoration: InputDecoration(
                                    focusedBorder: OutlineInputBorder(
                                        borderSide: BorderSide(
                                            color: Color(0xFF16B0C4)
                                                .withOpacity(0.7),
                                            width: 5.0),
                                        borderRadius: const BorderRadius.all(
                                            const Radius.circular(20.0))),
                                    enabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(
                                            color: Color(0xFF16B0C4)
                                                .withOpacity(0.7),
                                            width: 5.0),
                                        borderRadius: const BorderRadius.all(
                                            const Radius.circular(20.0))),
                                    //hintText: priceController.text,
                                    hintStyle: new TextStyle(
                                      fontFamily: 'Cairo',
                                      fontStyle: FontStyle.italic,
                                      fontWeight: FontWeight.normal,
                                      color: Colors.white,
                                    ),
                                  ),
                                )),
                          ),
                        )
                      ],
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    Container(
                      alignment: Alignment.topLeft,
                      padding: EdgeInsets.only(left: 30.0),
                      child: InkWell(
                        child: Text(
                          'Items Purchased',
                          style: TextStyle(
                            fontSize: 18.0,
                            color: Colors.white.withOpacity(0.8),
                            fontStyle: FontStyle.italic,
                            fontWeight: FontWeight.bold,
                            fontFamily: 'Montserrat',
                          ),
                        ),
                      ),
                    ),
                    new Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Expanded(
                          child: Padding(
                            padding: const EdgeInsets.only(
                                left: 20.0, right: 20.0, top: 10.0),
                            child: GestureDetector(
                                onTap: () {},
                                child: new TextField(
                                  controller: placeController,
                                  textAlign: TextAlign.center,
                                  obscureText: false,
                                  keyboardType: TextInputType.datetime,
                                  style: TextStyle(color: Colors.white),
                                  decoration: InputDecoration(
                                      focusedBorder: OutlineInputBorder(
                                          borderSide: BorderSide(
                                              color: Color(0xFF16B0C4)
                                                  .withOpacity(0.7),
                                              width: 5.0),
                                          borderRadius: const BorderRadius.all(
                                              const Radius.circular(20.0))),
                                      enabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(
                                              color: Color(0xFF16B0C4)
                                                  .withOpacity(0.7),
                                              width: 5.0),
                                          borderRadius: const BorderRadius.all(
                                              const Radius.circular(20.0))),
                                      //hintText: itemsController.text,
                                      hintStyle: new TextStyle(
                                        fontFamily: 'Cairo',
                                        fontStyle: FontStyle.italic,
                                        fontWeight: FontWeight.normal,
                                        color: Colors.white,
                                      )),
                                )),
                          ),
                        )
                      ],
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    Container(
                      alignment: Alignment.topLeft,
                      padding: EdgeInsets.only(left: 30.0),
                      child: InkWell(
                        child: Text(
                          'Date of Purchase',
                          style: TextStyle(
                            fontSize: 18.0,
                            color: Colors.white.withOpacity(0.8),
                            fontStyle: FontStyle.italic,
                            fontWeight: FontWeight.bold,
                            fontFamily: 'Montserrat',
                          ),
                        ),
                      ),
                    ),
                    new Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Expanded(
                          child: Padding(
                            padding: const EdgeInsets.only(
                                left: 20.0, right: 20.0, top: 10.0),
                            child: GestureDetector(
                                onTap: () {},
                                child: new TextField(
                                  controller: dateController,
                                  textAlign: TextAlign.center,
                                  keyboardType: TextInputType.text,
                                  obscureText: false,
                                  style: TextStyle(color: Colors.white),
                                  decoration: InputDecoration(
                                      focusedBorder: OutlineInputBorder(
                                          borderSide: BorderSide(
                                              color: Color(0xFF16B0C4)
                                                  .withOpacity(0.7),
                                              width: 5.0),
                                          borderRadius: const BorderRadius.all(
                                              const Radius.circular(20.0))),
                                      enabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(
                                              color: Color(0xFF16B0C4)
                                                  .withOpacity(0.7),
                                              width: 5.0),
                                          borderRadius: const BorderRadius.all(
                                              const Radius.circular(20.0))),
                                      //hintText: dateController.text,
                                      hintStyle: new TextStyle(
                                        fontFamily: 'Cairo',
                                        fontStyle: FontStyle.italic,
                                        fontWeight: FontWeight.normal,
                                        color: Colors.white,
                                      )),
                                )),
                          ),
                        )
                      ],
                    ),
                    SizedBox(
                      height: 25,
                    ),
                    new Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Expanded(
                          child: Padding(
                            padding: const EdgeInsets.only(
                                left: 60.0, right: 60.0, top: 10.0),
                            child: GestureDetector(
                              onTap: () {
                                showDialog(
                                  context: context,
                                  builder: (context) {
                                    Widget cancelButton = TextButton(
                                          child: Text("Cancel"),
                                          onPressed: () => Navigator.pop(context)
                                        );
                                        Widget continueButton = TextButton(
                                          child: Text("Continue"),
                                          onPressed:  () {
                                            createExcel();
                                          },
                                        );
                                     return AlertDialog(
                                        title: Text("Data Confirmation"),
                                        content: Text("Would you like to continue with the saved information?"), 
                                        actions: <Widget>[
                                          cancelButton,
                                          continueButton,
                                        ],
                                    );
                                  },
                                );
                              },
                              child: new Container(
                                  alignment: Alignment.center,
                                  height: 60.0,
                                  width: 50.0,
                                  decoration: new BoxDecoration(
                                      color: Color(0xFF16B0C4).withOpacity(0.7),
                                      borderRadius:
                                          new BorderRadius.circular(30.0)),
                                  child: new Text("Save changes",
                                      style: new TextStyle(
                                          fontSize: 20.0,
                                          color: Colors.white))),
                            ),
                          ),
                        )
                      ],
                    ),
                  ],
                ),
              ],
            )));
  }
}
